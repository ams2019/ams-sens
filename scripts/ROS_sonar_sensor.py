#!/usr/bin/env python

import signal
import sys
import time
import math

import rospy
import RPi.GPIO as GPIO
from sensor_msgs.msg import Range

global up

class sonar():
    def __init__(self):
        global up
        #Switch between the two directions
        if(up):
            name = '/sonar_dist_up'
        else:
            name = '/sonar_dist_down'
        #Create publisher
        self.distance_publisher = rospy.Publisher(name, Range, queue_size=10)
        self.r = rospy.Rate(15)
    def dist_sendor(self, dist):
        #Fill range data type and publish data
        data = Range()
        data.header.stamp = rospy.Time().now()
        data.header.frame_id = "map"
        data.range = dist/100
        data.min_range = 0.02
        data.max_range = 4
        data.field_of_view = math.radians(15)
        self.distance_publisher.publish(data)

def shutdown_handler():
    GPIO.cleanup()

#Init node
rospy.init_node('sonar', anonymous=True)

#Register shutdown handler
rospy.on_shutdown(shutdown_handler)

#Get orientation param
up = rospy.get_param("~sonar_up")

#Pin setup
GPIO.setmode(GPIO.BOARD)
if(up):
    TRIG_UP = 22
    ECHO_UP = 24
else:
    TRIG_UP = 16
    ECHO_UP = 18

GPIO.setup(TRIG_UP, GPIO.OUT)
GPIO.setup(ECHO_UP, GPIO.IN)

#Start sonar
sensor = sonar()
time.sleep(0.5)
try:
    while not rospy.is_shutdown():
        #Trigger Sensor
        GPIO.output(TRIG_UP, False)
        time.sleep(0.1)
        GPIO.output(TRIG_UP, True)
        time.sleep(0.00001)
        GPIO.output(TRIG_UP, False)
        #Wait for echo
        while GPIO.input(ECHO_UP) == 0:
            pulse_start = time.time()
        while GPIO.input(ECHO_UP) == 1:
            pulse_end = time.time()
        #Calculate  distance
        pulse_duration = pulse_end - pulse_start
        distance = pulse_duration * 17160
        if pulse_duration >= 0.01746:
            distance = -1
        elif distance > 300 or distance == 0:
            distance = -1
        distance = round(distance, 3)
        sensor.dist_sendor(distance)
        sensor.r.sleep()
except (KeyboardInterrupt, SystemExit):
    GPIO.cleanup()
    sys.exit()