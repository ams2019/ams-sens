#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import LaserScan, Range
import sys

global dist_up
dist_up = -1.0
global dist_down
dist_down = -1.0
global laser_range
laser_range = -1.0

def callback_up(data):
	global dist_up
	dist_up = data.range

def callback_down(data):
	global dist_down
	dist_down = data.range

def callback_lidar(data):
	global laser_range
	laser_range = data.ranges[0]

def listener():
	rospy.init_node('sensor_client', anonymous=True)

	rospy.Subscriber("sonar_dist_up", Range, callback_up)
	rospy.Subscriber("sonar_dist_down", Range, callback_down)
	rospy.Subscriber("/scan", LaserScan, callback_lidar)

	#rospy.spin()

if __name__ == '__main__':
	listener()
	time.sleep(0.5)
	try:
		while not rospy.is_shutdown():
			time.sleep(0.1)
			rospy.loginfo("Up: {0}, Down: {1}, Forward: {2}".format(dist_up, dist_down, laser_range))
	except(KeyboardInterrupt, SystemExit):
		sys.exit()
