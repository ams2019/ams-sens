#!/bin/bash

# LOG
LOG=/home/pi/ams-sens/logs/ultrasonic.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash

# ULTRASONIC
/home/pi/ros-hc-sr04-node/src/export_gpio_pins.sh start
unbuffer rosrun hc_sr04 hc_sr04_node 2>&1 | tee $LOG
