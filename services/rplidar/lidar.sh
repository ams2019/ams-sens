#!/bin/bash

# LOG
LOG=/home/pi/ams-sens/logs/rplidar.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash

# IMU CALIBRATION
MSG="Calibrate IMU before starting LIDAR"
echo $MSG >> $LOG
rosservice call calibrate_imu

# RPLIDAR
unbuffer roslaunch rplidar_ros rplidar.launch 2>&1 | tee $LOG
