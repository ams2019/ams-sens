#!/bin/bash

# LOG
LOG=/home/pi/ams-sens/logs/rplidar.log
MSG="Stopping lidar motor now"
echo $MSG >> $LOG

# ROS
source /home/pi/catkin_ws/devel/setup.bash
ROS="$(rosversion -d)"
source /opt/ros/$ROS/setup.bash
echo $MSG
rosservice call /stop_motor