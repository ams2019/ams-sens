# Simple test for NeoPixels on Raspberry Pi
import time
import board
import neopixel

last_millis = 0
blink_interval = 500
blink_status = False

# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 8

# The brightness of NeoPixels
brightness=0.2

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

# Define NeoPixels
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=brightness, auto_write=False,
                           pixel_order=ORDER)

# Define colors
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
OFF = (0, 0, 0)

# Define status colors (off, red, yellow, green)
colorArray = [OFF, RED, YELLOW, GREEN]

# Define Component Status
OFFLINE = 0
ERROR = 1
LOADING = 2
READY = 3

# Define Status Array
statusArray = [OFFLINE, OFFLINE, OFFLINE, OFFLINE, OFFLINE, OFFLINE, OFFLINE, OFFLINE]

# Define Components
OS = 0
ROS = 1
FC = 2
RC = 3
IMU = 4
SONIC = 5
LIDAR = 6
ARM = 7

# Set status for component in array
def setStatus(component, status):
    statusPos = getStatusPos(component)
    colorPos = getColorPos(status)
    statusArray[statusPos] = colorPos

# Get status from component array
def getStatus(component):
    position = getStatusPos(component)
    return statusArray[position]

# Get status position in component array
def getStatusPos(component):
    return {
        'OS': 0,
        'ROS': 1,
        'FC': 2,
        'RC': 3,
        'IMU': 4,
        'SONIC': 5,
        'LIDAR': 6,
        'ARM': 7
    }[component]

# Get color position from color array
def getColorPos(status):
    return {
        'OFFLINE': 0,
        'ERROR': 1,
        'LOADING': 2,
        'READY': 3
    }[status]

# draw pixels with current status
def showPixels():
    for i in range(len(pixels)):
        status = statusArray[i]
        color = colorArray[status]
        pixels[i] = color
    pixels.show()

# use millis to blink async status
def blinkStatus(component, status):
    global last_millis
    global blink_status
    millis = int(round(time.time() * 1000))

    if millis > last_millis + blink_interval:
        blink_status = not blink_status
        last_millis = millis

    if blink_status == True:
        status = 'OFFLINE'
    
    setStatus(component, status)

# check status of components
def checkStatus():
    #setStatus('OS', 'READY')
    blinkStatus('OS', 'READY')

# Loop forever and show the colors
print("Start System Status Check")
while True:
    checkStatus()
    showPixels()
