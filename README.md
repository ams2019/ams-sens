# AMS Sensors

This repository contains all code used for sensor reading and actuator controlling.  

## Requirements

### Catkin

Setup catkin workspace without adding any package to source folder:

1. Setup catkin workspace in `~/catkin_ws`: [ROS instructions](https://wiki.ros.org/catkin/Tutorials/create_a_workspace)
2. Clone packages to `~/catkin_ws/src`
3. Run `catkin_make` in `~/catkin_ws`
4. Add to your `.bashrc`: `source ~/catkin_ws/devel/setup.bash`

### WiringNP (NanoPi)

This is a GPIO access library for NanoPi. It is based on the WiringOP for Orange PI which is based on original WiringPi for Raspberry Pi.

    cd ~
    git clone https://github.com/friendlyarm/WiringNP
    cd WiringNP/
    chmod 755 build
    ./build

### WiringPi (Raspberry Pi)

WiringPi is a PIN based GPIO access library written in C for the BCM2835, BCM2836 and BCM2837 SoC devices used in all Raspberry Pi.

    cd ~
    git clone https://github.com/WiringPi/WiringPi.git
    cd WiringPi
    ./build

Test installation with

    gpio -v
    gpio readall

## How to use

### Register as Linux-Service

To register the components as linux service, link the following files:  
(This adds symlinks at `/etc/systemd/system`)

    sudo systemctl link ~/ams-sens/services/rplidar/lidar.service
    sudo systemctl link ~/ams-sens/services/ultrasonic/sonic.service
    sudo systemctl link ~/ams-sens/services/ws2812LED/led.service

Start/Stop/Restart service with

    sudo service {service-name} {start/stop/restart}

After making changes on service-file restart `systemctl` with

    sudo systemctl daemon-reload

Then restart service with

    sudo service {service-name} restart

## Sensors

### Ultrasonic Sensor (HC-SR04)

For reading HC-SR04 Ultrasonic Sensors an implementation in python and c++ is available.  
It is recommended to use the c++ one, as the python one isn't stable in long term sensing.

- [Product-Link](https://www.sparkfun.com/products/15569)
- [Datasheet](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf)
- [Python Source](https://github.com/engcang/HC-SR04-UltraSonicSensor-ROS-RaspberryPi)
- [C++ Source](https://github.com/matpalm/ros-hc-sr04-node)

#### Python Implementation (unstable)

Distance calculations happens in node `sonar`.  
Set global variable `~sonar_up` to differentiate between the upward and downward facing sensor.  
Use `sonar_up=true` for upwards-sensor and `sonar_up=false` for downwards-sensor.  
`sonar`-Node is started twice, to get data from both directions.

##### Installation

1. Clone `ams-sens` into home directory

        cd ~
        git clone https://Rafael91@bitbucket.org/ams2019/ams-sens.git

2. Set the desired GPIO pins

    - `ams-sens/scripts/ROS_sonar_sensor.py` (GPIO BOARD Layout)

3. Create symlink from `ams-sens` folder to catkin workspace

        sudo ln -s ~/ams-sens ~/catkin_ws/src/ams-sens

4. Enter catkin workspace and build

        cd ~/catkin_ws
        catkin_make

##### How to use

Start manually:

    roslaunch ams-sens sonar.launch

Test all sensor-publishes:

    rosrun ams-sens ROS_sensor_client.py

##### Published Topics

- `/sonar_dist_up`
- `/sonar_dist_down`

Test published topics:

    rostopic echo sonar_dist_down
    rostopic echo sonar_dist_up

#### C++ Implementation (stable)

Distance calculations happens in node `hc_sr04_node`.  
It is required to run `src/export_gpio_pins.sh start` to export pins.  
Otherwise it needs to be run as root (limitation of wiringPiSetupSys).  

##### Installation

1. Clone rafaelmaeuer's fork of `ros-hc-sr04-node` into home directory:

        cd ~
        git clone https://github.com/rafaelmaeuer/ros-hc-sr04-node.git

2. Set the desired GPIO pins

    - `ros-hc-sr04-node/src/export_gpio_pins.sh` (GPIO BCM Layout)
    - `ros-hc-sr04-node/src/hc_sr04_node.cpp` (GPIO Board Layout)

3. Create symlink from `ros-hc-sr04-node` folder to catkin workspace

        sudo ln -s ~/ros-hc-sr04-node ~/catkin_ws/src/ros-hc-sr04-node

4. Enter catkin workspace and build

        cd ~/catkin_ws
        catkin_make

##### How to use

Start manually:

    export_gpio_pins.sh start
    rosrun hc_sr04 hc_sr04_node

Start as service (see `Register as Linux-Service` in ams-cc/README.md):

    sudo service sonic start

##### Published Topics

- `/sonar_0` (down)
- `/sonar_1` (up)

Test published topics:

    rostopic echo sonar_0
    rostopic echo sonar_1

#### Published Topic Details of HC-SR04

sensor_msgs/Range.msg:

```sh
Header header           # timestamp in the header is the time the ranger
                        # returned the distance reading

# Radiation type enums
# If you want a value added to this list, send an email to the ros-users list
uint8 ULTRASOUND=0
uint8 INFRARED=1

uint8 radiation_type    # the type of radiation used by the sensor
                        # (sound, IR, etc) [enum]

float32 field_of_view   # the size of the arc that the distance reading is
                        # valid for [rad]
                        # the object causing the range reading may have
                        # been anywhere within -field_of_view/2 and
                        # field_of_view/2 at the measured range. 
                        # 0 angle corresponds to the x-axis of the sensor.

float32 min_range       # minimum range value [m]
float32 max_range       # maximum range value [m]
                        # Fixed distance rangers require min_range==max_range

float32 range           # range data [m]
                        # (Note: values < range_min or > range_max
                        # should be discarded)
                        # Fixed distance rangers only output -Inf or +Inf.
                        # -Inf represents a detection within fixed distance.
                        # (Detection too close to the sensor to quantify)
                        # +Inf represents no detection within the fixed distance.
                        # (Object out of range)
```

### LIDAR Sensor (RPILIDAR A2)

- [Product-Link](https://www.slamtec.com/en/Lidar/A2)
- [Datasheet](http://bucket.download.slamtec.com/d00df2d983fa1f47a24c56d489f4b4917773fb45/LD208_SLAMTEC_rplidar_datasheet_A2M8_v2.5_en.pdf)
- [Git-Package](https://github.com/slamtec/rplidar_ros)
- [ROS Wiki package](https://wiki.ros.org/rplidar)

#### Installation

1. Clone `rplidar_ros` into home directory

        cd ~
        git clone https://github.com/Slamtec/rplidar_ros.git

2. Set the correct serial port
    - `rplidar_ros/launch/rplidar.launch` (/dev/ttyUSB{X})

3. Create symlink from `rplidar_ros` folder to catkin workspace

        sudo ln -s ~/rplidar_ros ~/catkin_ws/src/rplidar_ros

4. Enter catkin workspace and build

        cd ~/catkin_ws
        catkin_make

#### How to use

Start manually:

    roslaunch rplidar_ros rplidar.launch

Start as service (see service setup in ams-cc/README.md):

    sudo service lidar start

Manual motor control:

    rosservice call /stop_motor
    rosservice call /start_motor

#### Published Topics

- `/scan`

#### Published Topic Details of RPLidar

sensor_msgs/LaserScan.msg:

```sh
Header header            # timestamp in the header is the acquisition time of
                         # the first ray in the scan.
                         #
                         # in frame frame_id, angles are measured around
                         # the positive Z axis (counterclockwise, if Z is up)
                         # with zero angle being forward along the x axis

float32 angle_min        # start angle of the scan [rad]
float32 angle_max        # end angle of the scan [rad]
float32 angle_increment  # angular distance between measurements [rad]

float32 time_increment   # time between measurements [seconds] - if your scanner
                         # is moving, this will be used in interpolating position
                         # of 3d points
float32 scan_time        # time between scans [seconds]

float32 range_min        # minimum range value [m]
float32 range_max        # maximum range value [m]

float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
float32[] intensities    # intensity data [device-specific units].  If your
                         # device does not provide intensities, please leave
                         # the array empty.
```

## Actuators

### WS2812 LED

- [Product-Link](https://hobbyking.com/en_us/ws2812-led-light-board-for-cc3d-and-naze32.html)
- [Datasheet](https://cdn-shop.adafruit.com/datasheets/WS2812.pdf)

To enable the WS2812 LED status-bar follow the instructions below

#### WS2812 on NanoPi Neo Air

1. Install Scons

        sudo apt-get install scons

2. Clone rafaelmaeuer's fork of rpi_ws281x repository

        git clone https://github.com/rafaelmaeuer/rpi_ws281x.git

3. Replace `main.c` with symlink from this repository

        cd ~/rpi_ws281x
        rm main.c
        sudo ln -s ~/ams-sens/services/ws2812LED/main.c ~/rpi_ws281x/main.c

4. Build led binary

        scons

5. Test led binary

        sudo ./led

6. Create the following symlink:

        sudo systemctl link ~/ams-sens/services/ws2812LED/led.service

#### Neopixel on Raspberry Pi

- [Raspberry Pi Wiring](https://learn.adafruit.com/neopixels-on-raspberry-pi/raspberry-pi-wiring)
- [Prerequisite Pi Setup](https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi)
- [Python Usage](https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage)

Create the following symlink:

    sudo systemctl link ~/ams-sens/services/neopixel/neopixel.service

## Logs

All services writes logs to the `logs` folder. To get the output of all logfiles run the monitor script

    ~/ams-sens/logs/monitor.sh
